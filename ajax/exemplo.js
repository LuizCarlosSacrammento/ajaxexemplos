document.getElementById("busca").onclick=function(){
	var xmlHttp = new XMLHttpRequest();
      	xmlHttp.onreadystatechange = function(){
		if(xmlHttp.readyState===4&&xmlHttp.status===200)
			document.getElementById("retorno").innerHTML = xmlHttp.responseText;
	};
	xmlHttp.open("GET","teste.txt",true);
	xmlHttp.send();
};	

//Explicação: busquei o id do botão quando o click for acionado
//	"document.getElementById("busca").onclick=function()"
//atribui à variável "xmlHttp" o objeto que nos ajuda a fazer a comunicação com o servidor 
//depois atribui o evento a variável "onreadystatechange"

//Já é uma métric própria do "onreadystatechange" que quando o "readyState" é 4 a requisição 
//foi finalizada e ele está pronto para responder, e "status" é igual a 200 não houve erro algum

//	"document.getElementById("retorno")" busca o elemento do lugar onde quero 
//	"innerHTML" faz um conteúdo se formatado na estrutura em HTML, então é dito que o conteúdo será
//igual a virável "xmlHttp" e o "responseText", ou seja, o conteúdo que o servidor retorna. No caso é responseText
//porque estamos trabalhando com o arquivo de texto

//"xmlHttp.open" ele tem como responsabilidade a busca do seu arquivo

//GET = mais rápido e mais recomendável para busca de conteúdo
// true -> Faz com que a requisição continue mesmo antes de ser retornada
//"xmlHttp.send();" vai enviar a requisição 